import React, { Component } from "react";
import { connect } from "react-redux";
import { ADD_CART } from "./Redux/constan/shoeContant";

class Item_Shoe extends Component {
  render() {
    let { image, name, price } = this.props.item;
    return (
      <div className="card col-4" style={{ width: "18rem" }}>
        <img className="card-img-top" src={image} alt="Card image cap" />
        <div className="card-body">
          <h5 className="card-title">{name}</h5>
          <p className="card-text">{price}</p>
          <a
            onClick={() => {
              this.props.handleAddCat(this.props.item);
            }}
            href="#"
            className="btn btn-primary"
          >
            Add
          </a>
        </div>
      </div>
    );
  }
}

let mapDispatchToProps = (dispatch) => {
  return {
    handleAddCat: (shoe) => {
      let action = {
        type: ADD_CART,
        payload: shoe,
      };
      dispatch(action);
    },
  };
};

export default connect(null, mapDispatchToProps)(Item_Shoe);
