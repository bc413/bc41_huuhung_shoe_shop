import React, { Component } from "react";
import { connect } from "react-redux";
import Item_Shoe from "./Item_Shoe";

class List_Shoe extends Component {
  renderShoe = () => {
    return this.props.dataShoe.map((item, index) => {
      return <Item_Shoe item={item} key={index} />;
    });
  };
  render() {
    return (
      <div>
        <h5>List_Shoe</h5>
        <div className="row">{this.renderShoe()}</div>
      </div>
    );
  }
}

let mapStateToProps = (state) => {
  return {
    dataShoe: state.phoneReducer.dataShoe,
  };
};

export default connect(mapStateToProps)(List_Shoe);
