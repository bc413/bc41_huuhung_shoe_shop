import { data_shoe } from "../Data_Shoe";
import { ADD_CART, DELETE, UP_DATE_SL } from "./constan/shoeContant";

let initiaValue = {
  dataShoe: data_shoe,
  cart: [],
};

export let phoneReducer = (state = initiaValue, action) => {
  switch (action.type) {
    case ADD_CART: {
      let newCart = [...state.cart];
      let index = newCart.findIndex((item) => {
        return item.id === action.payload.id;
      });
      if (index == -1) {
        let newObject = { ...action.payload, soLuong: 1 };
        newCart.push(newObject);
      } else {
        newCart[index].soLuong++;
      }
      return { ...state, cart: newCart };
    }
    case DELETE: {
      let newCart = [...state.cart];
      let arrNew = newCart.filter((item) => {
        return item.id !== action.payload;
      });
      return { ...state, cart: arrNew };
    }
    case UP_DATE_SL: {
      let newCart = [...state.cart];
      let index = newCart.findIndex((item) => {
        return item.id === action.payload.idShoe;
      });
      newCart[index].soLuong += action.payload.capNhat;
      if (newCart[index].soLuong < 1) {
        newCart.splice(index, 1);
      }
      return { ...state, cart: newCart };
    }
    default:
      return state;
  }
};
