import { combineReducers } from "redux";
import { phoneReducer } from "./phoneReducer";

export let rootReducer_Shoe_Shop = combineReducers({
  phoneReducer: phoneReducer,
});
