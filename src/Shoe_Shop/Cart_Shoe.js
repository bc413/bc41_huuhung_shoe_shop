import React, { Component } from "react";
import { connect } from "react-redux";
import { DELETE, UP_DATE_SL } from "./Redux/constan/shoeContant";

class Cart_Shoe extends Component {
  renderCart = () => {
    return this.props.dataCart.map((item) => {
      return (
        <tr>
          <td>{item.id}</td>
          <td>{item.name}</td>
          <td>{item.price * item.soLuong}$</td>
          <td>
            <button
              onClick={() => {
                this.props.handleUpdateSL(item.id, 1);
              }}
              className="btn btn-primary"
            >
              +
            </button>
            <strong className="mr-2 ml-2">{item.soLuong}</strong>
            <button
              onClick={() => {
                this.props.handleUpdateSL(item.id, -1);
              }}
              className="btn btn-primary"
            >
              -
            </button>
          </td>
          <td>
            <img style={{ width: "100px" }} src={item.image} alt="" />
          </td>
          <td>
            <button
              onClick={() => {
                this.props.handleDelete(item.id);
              }}
              className="btn btn-danger"
            >
              DeLeTe
            </button>
          </td>
        </tr>
      );
    });
  };
  render() {
    return (
      <div>
        <h5>Cart_Shoe</h5>
        <table style={{ width: "100%" }}>
          <thead>
            <tr>
              <th>ID</th>
              <th>Name</th>
              <th>Price</th>
              <th>SL</th>
              <th>Image</th>
              <th>Action</th>
            </tr>
          </thead>
          <tbody>{this.renderCart()}</tbody>
        </table>
      </div>
    );
  }
}

let mapStateToProps = (state) => {
  return {
    dataCart: state.phoneReducer.cart,
  };
};

let mapDispatToProps = (dispatch) => {
  return {
    handleDelete: (idShoe) => {
      let action = {
        type: DELETE,
        payload: idShoe,
      };
      dispatch(action);
    },
    handleUpdateSL: (idShoe, capNhat) => {
      let action = {
        type: UP_DATE_SL,
        payload: { idShoe, capNhat },
      };
      dispatch(action);
    },
  };
};

export default connect(mapStateToProps, mapDispatToProps)(Cart_Shoe);
